const User =  require('../models/Users.js') 
const bcrypt = require('bcrypt') 
const auth = require('../auth')
const Product = require('../models/Product.js')

module.exports.userRegister = (reqBody) => {
		let newUser = new User({
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			password: bcrypt.hashSync(reqBody.password, 10)

		})

		return newUser.save().then((saveUser,error)=>{
			if(error){
				return error
			}else{
				return 'New user created successfully'
			}
		})

}


module.exports.userLogin = (reqBody) =>{

	return User.findOne({email: reqBody.email}).then((result) => {
		if(result == null){
			return "User is not registered"
		}else{
			const isPassword = bcrypt.compareSync(reqBody.password,result.password)

			if(isPassword){
				return {acces: auth.createAccessToken(result)}
			}else{
				return "User is not registered"
			}
		}

	})
}


module.exports.addProduct = (reqBody) => {


	let newProduct = new Product ({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive

	})
		return newProduct.save().then((course, error) => {
			
			if(error) {
					return false
			} else {
			
			return newProduct
		}
	})
}


module.exports.allProduct = async (user) => {
	
		return Product.find({isActive: true}).then((result,error) =>{
			if(error){
				return "error request"
			}else{
				return result
			}
		})
	

}

module.exports.singleProduct = async (user) => {

			return Product.findById(user.productId).then((result,error) => {
				if(error){
					return error
				}else{
					return result
				}
			})
		


}

