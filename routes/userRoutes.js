const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController.js')
const auth = require('../auth')


router.post('/register', (request,response) => {
		userController.userRegister(request.body).then((user) => response.send(user))
}) 


router.post('/login',(request,response) => {
	userController.userLogin(request.body).then((user) => response.send(user))
})


router.post('/product', auth.verify, (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	
	if(userData.isAdmin) {
		userController.addProduct(request.body).then(result => response.send(result))
	} else {
		return response.send("User is not authorized")
	}
})


router.get('/all', auth.verify, (request,response) => {
	
	let data = {
		userId : auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

	
	userController.allProduct(data).then(result => response.send(result))
})


router.get('/single-product', auth.verify, (request,response) => {

	let data = {
		userId : auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		productId: request.body.productId
		}

	userController.singleProduct(data).then(result => response.send(result))

})


module.exports = router 