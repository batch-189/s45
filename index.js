
//Require section 
const express = require('express')
const mongoose = require('mongoose')
const userRoutes = require('./routes/userRoutes')


//Port section
const PORT = process.env.PORT || 3000
const app = express()


//application section
app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.use('/users', userRoutes)

//Mongoose Connection 
mongoose.connect(`mongodb+srv://admin12345:admin123@zuitt-bootcamp.qquvkqu.mongodb.net/Capstone2?retryWrites=true&w=majority`,{
			useNewUrlParser: true,
			useUnifiedTopology: true
})

let db = mongoose.connection
db.on('error', () => console.error.bind(console, 'error'))
db.once(`open`, () => console.log(`Now connected in mongoDB Atlas`))



app.listen(PORT, () =>  {
	console.log(`API is now online on port ${PORT}`)
})


